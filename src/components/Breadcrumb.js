import React from 'react';
import { Link, withRouter } from "react-router-dom"

import { Breadcrumb } from 'antd';

const breadcrumbNameMap = {//跟路由路径保持一致
  "/": "首页",
  "/403": "403",
  "/404": "404",
  "/basic": "React基础",
  "/basic/lifeCycle": "React生命周期",
  "/basic/useStateHook": "UseStateHook",
  "/basic/home": "Home",
  "/basic/about": "About",
  "/component": "Antd组",
  "/component/testForm": "表单",
  "/component/treeTable": "树形表格",
  "/component/testModel": "Modal对话框"
};

const breadcrumb = (props) => {
  const { location } = props;
  const pathSnippets = location.pathname.split("/").filter((i) => i);
  const extraBreadcrumbItems = pathSnippets.map((_, index) => {
      const url = `/${pathSnippets.slice(0, index + 1).join("/")}`;
      return (
          <Breadcrumb.Item key={url}>
              <Link to={url}>
                { breadcrumbNameMap[url] }
              </Link>
          </Breadcrumb.Item>
      );
  });
  const breadcrumbItems = [
    <Breadcrumb.Item key="home"><Link to="/basic">首页</Link></Breadcrumb.Item>
  ].concat(extraBreadcrumbItems);
  return (
    <Breadcrumb style={{ margin: '16px 0' }}>
       { breadcrumbItems }
    </Breadcrumb>
  );
};
  
export default withRouter(breadcrumb);