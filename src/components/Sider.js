import React from 'react';
import { Link } from 'react-router-dom';
import { Layout, Menu } from 'antd';
import 'antd/dist/antd.css';
import { HomeOutlined, StarOutlined, TableOutlined, FileTextOutlined } from '@ant-design/icons';

const { SubMenu } = Menu;
const { Sider } = Layout;

const sider = () => {
  return (
    <Sider width={200} collapsible={true} className="site-layout-background">
        <Menu
          mode="inline"
          defaultSelectedKeys={['101']}
          defaultOpenKeys={['sub1']}
          style={{ height: '100%', borderRight: 0 }}
        >
          <SubMenu key="sub1" icon={<HomeOutlined />} title="React基础">
            <Menu.Item key="101"><Link to="/basic/home">Home</Link></Menu.Item>
            <Menu.Item key="102"><Link to="/basic/about">About</Link></Menu.Item>
            <Menu.Item key="103"><Link to="/basic/lifeCycle">React生命周期</Link></Menu.Item>
            <Menu.Item key="104"><Link to="/basic/useStateHook">UseStateHook</Link></Menu.Item>
          </SubMenu>
          <SubMenu key="sub2" icon={<StarOutlined />} title="Antd组件">
            <Menu.Item key="201" icon={<FileTextOutlined />}><Link to="/component/testForm">表单</Link></Menu.Item>
            <Menu.Item key="202" icon={<TableOutlined />}><Link to="/component/treeTable">树形表格</Link></Menu.Item>
            <Menu.Item key="203" icon={<FileTextOutlined />}><Link to="/component/testModel">Modal对话框</Link></Menu.Item>
          </SubMenu>
        </Menu>
    </Sider>
  );
};

export default sider;