import React from 'react';
import { Link } from 'react-router-dom';
import { Layout, Menu } from 'antd';
import { Avatar } from 'antd';
import { Row, Col } from 'antd';
import { Dropdown, Button } from 'antd';
import { DownOutlined, LogoutOutlined } from '@ant-design/icons';
import 'antd/dist/antd.css';

import userJpg from '../assets/user.jpg'; 

const { Header } = Layout;

const menu = (
  <Menu>
    <Menu.Item icon={<LogoutOutlined />}>
      <Link to="/login">退出</Link>
    </Menu.Item>
  </Menu>
);

const header = () => {
  return (
    <Header className="header">
      <Row>
        <Col span={20}>
        <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['2']}>
          <Menu.Item key="1">nav 1</Menu.Item>
          <Menu.Item key="2">nav 2</Menu.Item>
          <Menu.Item key="3">nav 3</Menu.Item>
        </Menu>
        </Col>
        <Col span={4}>
          <Dropdown overlay={menu}>
            <Button ghost>
              Antonio<DownOutlined />
            </Button>
          </Dropdown>
          <Avatar src={ userJpg } />
        </Col>
      </Row>
    </Header>
  );
};
  
export default header;