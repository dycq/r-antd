import React from 'react';
import {BrowserRouter} from 'react-router-dom';
import { Layout } from 'antd';
import 'antd/dist/antd.css';

import Breadcrumb from './components/Breadcrumb.js';
import Header from './components/Header.js';
import Sider from './components/Sider.js';

import Routes from './Routes.js';

const { Content } = Layout;

function App() {
  return (
    <BrowserRouter>
      <Layout>
        <Header />
        <Layout>
          <Sider />
          <Layout style={{ padding: '0 24px 24px' }}>
            <Breadcrumb />
            <Content
              className="site-layout-background"
              style={{
                padding: 24,
                margin: 0,
                minHeight: document.documentElement.clientHeight - 150
              }}
            >
              <Routes />
            </Content>
          </Layout>
        </Layout>
      </Layout>
    </BrowserRouter>
  );
}

export default App;
