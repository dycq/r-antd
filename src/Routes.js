import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

import MainLayout from './layout/MainLayout.js';
import Login from './pages/Login.js';
import Register from './pages/Register.js';
import Home from './pages/basic/Home.js';
import About from './pages/basic/About.js';
import UseStateHook from './pages/basic/UseStateHook.js';
import LifeCycle from './pages/basic/LifeCycle.js';
import TestForm from './pages/test/TestForm.js';
import TreeTable from './pages/test/TreeTable.js';
import TestModel from './pages/test/TestModel.js';


import NotFound from './pages/NotFound.js';

const Routes = () => (
  <div>
    <Switch>
      <Route path="/" exact>
        <Redirect from="/" to="/basic" exact />
      </Route>
      <Route path="/login" exact component={Login} />
      <Route path="/register" exact component={Register} />
      <MainLayout>
      <Route path="/basic" exact>
        <Redirect from="/basic" to="/basic/home" exact />
      </Route>
      <Route path="/basic/lifeCycle" exact component={LifeCycle} />
      <Route path="/basic/useStateHook" exact component={UseStateHook} />
      <Route path="/basic/home" exact component={Home} />
      <Route path="/basic/about" exact component={About} />
      <Route path="/component" exact>
        <Redirect from="/component" to="/component/testForm" exact />
      </Route>
      <Route path="/component/testForm" exact component={TestForm} />
      <Route path="/component/treeTable" exact component={TreeTable} />
      <Route path="/component/testModel" exact component={TestModel} />
      </MainLayout>
      <Route path="*" component={NotFound} />
    </Switch>
  </div>
);

export default Routes;
