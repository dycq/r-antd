import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import { Row, Col, Card, Form, Input, Button } from 'antd';

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
};
const tailLayout = {
  wrapperCol: { offset: 8, span: 16 },
};

const Register = (props) => {
  const onFinish = values => {
    console.log('Success:', values);
    props.history.push('/login');
  };

  const onFinishFailed = errorInfo => {
    console.log('Failed:', errorInfo);
  };

  return (
    <>
    <Row gutter={16} style={{marginTop:160}}>
      <Col span={8}></Col>
      <Col span={8}>
      <Card
        title="注册"
        bordered={true}
        extra={<Link to="/login">登录</Link>}
      >
        <Form
        {...layout}
        name="basic"
        initialValues={{ username: "Antonio", password: "123456" }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        >
        <Form.Item
            label="用户名"
            name="username"
            rules={[{ required: true, message: '请输入用户名!' }]}
        >
            <Input />
        </Form.Item>

        <Form.Item
            label="密码"
            name="password"
            rules={[{ required: true, message: '请输入密码' }]}
        >
            <Input.Password />
        </Form.Item>


        <Form.Item {...tailLayout}>
            <Button type="primary" htmlType="submit">
            提交
            </Button>
        </Form.Item>
        </Form>
        </Card>
      </Col>
      <Col span={8}></Col>
    </Row>
    </>
  );
};

export default withRouter(Register);