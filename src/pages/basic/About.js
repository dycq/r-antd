import React from 'react';
import PropTypes from 'prop-types';
import { Card } from 'antd';

class HelloMessage extends React.Component {
  render() {
    return (
      <h1>Hello, {this.props.name}</h1>
    );
  }
}
 
HelloMessage.defaultProps = {
  name: 'Antonio'
};

HelloMessage.propTypes = {
  name: PropTypes.string
};

const About = () => {
  return (
    <>
      <Card title="About" bordered={false} style={{ width: 300 }}>
        <HelloMessage />
        <HelloMessage name="Tom"/>
        <HelloMessage name={"Jerry"} />
        <HelloMessage name={1} />
      </Card>
    </>
  );
};

export default About;
