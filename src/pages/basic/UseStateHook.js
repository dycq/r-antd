import React, { useState } from 'react';

export default function UseStateHook() {
    let initialCount = 0;
    const [count, setCount] = useState(initialCount);
    return (
      <>
        <div></div>
        <div>Count: {count}</div>
        <div>
        <button onClick={() => setCount(initialCount)}>Reset</button>
        <button onClick={() => setCount(prevCount => prevCount - 1)}>-</button>
        <button onClick={() => setCount(prevCount => prevCount + 1)}>+</button>
        </div>
      </>
    );
}