import React from 'react';

const element = <h1>Hello, world!</h1>;

const Home = () => {
  return (
    <>
      <div>Home</div>
      { element }
    </>
  );
};

export default Home;
