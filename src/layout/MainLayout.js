import React, { Component } from 'react';
import { Layout } from 'antd';
import 'antd/dist/antd.css';

import Breadcrumb from '../components/Breadcrumb.js';
import Header from '../components/Header.js';
import Sider from '../components/Sider.js';

const { Content } = Layout;

export default class MainLayout extends Component {
    render() {
        return (
         <Layout>
            <Header />
            <Layout>
              <Sider />
              <Layout style={{ padding: '0 24px 24px' }}>
                <Breadcrumb />
                <Content
                  className="site-layout-background"
                  style={{
                    padding: 2,
                    margin: 0,
                    minHeight: document.documentElement.clientHeight - 142
                  }}
                >
                  { this.props.children }
                </Content>
              </Layout>
            </Layout>
          </Layout>
        )
    }
}