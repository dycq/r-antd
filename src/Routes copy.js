import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

import Home from './pages/Home.js';
import About from './pages/About.js';
import TestForm from './pages/test/TestForm.js';
import TreeTable from './pages/test/TreeTable.js';

import NotFound from './pages/NotFound.js';

const Routes = () => (
  <div>
    <Switch>
      <Route path="/">
        <Redirect from="/" to="/basic/home" exact />
        <Route path="/basic">
          <Switch>
            <Redirect from="/basic" to="/basic/home" exact />
            <Route path="/basic/home" exact component={Home} />
            <Route path="/basic/about" exact component={About} />
          </Switch>
        </Route>
        <Route path="/component">
          <Switch>
            <Redirect from="/component" to="/component/testForm" exact />
            <Route path="/component/testForm" exact component={TestForm} />
            <Route path="/component/treeTable" exact component={TreeTable} />
          </Switch>
        </Route>
      </Route>
      <Route path="*" component={NotFound} />
    </Switch>
  </div>
);

export default Routes;
